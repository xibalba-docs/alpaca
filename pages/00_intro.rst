============
Introducción
============

Un particularidad de la ingeniería informática es la necesidad de emplear tecnologías y paradigmas diversos. Las aplicaciones modernas potenciadas por PHP emplean diversos gestores de bases de datos para el almacenamiento y gestión de los mismos, planteando la necesidad de conectar dos tecnologías distintas: el propio PHP por una parte y el gestor por otra. Tal conexión pasa por la necesidad de plantear en un paradigma (POO en PHP) lo expresado en otro (declarativo en SQL). Los ORM implementan dicha capa, proveyendo de una abstracción que provee clases y objetos para la manipulación de datos almacenados en los gestores relacionales.

Existen dos patrones generales para la implementación de los ORM: el *Active Record* y el *Data Mapper*. Alpaca es un ORM ligero que implementa una forma del patrón Data Mapper. Alpaca provee una interfaz sencilla delegada en dos entidades: **Modelos** (``Model``) y **Guardianes** (``Keeper``). Toda la astracción que una persona necesita hacer sobre tablas y operaciones SQL puede plantearse en términos de Modelos y Guardianes.

Alpaca construye estructuras de datos internas (arreglos) y delega en Tuza la construcción de SQL y la intermediación con PDO y finalmente el gestor.

No obstante lo anterior, existen escenarios donde es necesario crear manualmente el SQL, Alpaca permite mediante los Guardianes proveer de un mecanismo que simplifique la creación de SQL y exponer en última instancia un método estático.

Arquitectura
============

Alpaca provee tres clases fundamentales: ``DbConnection``, ``Keeper`` y ``Model``, además de una clase de Excepción. Alpaca es muy fácil de utilizar, basta con crear una clase que herede de ``Model`` mapeando una tabla, y una clase que herede de ``Keeper`` e implemente el método ``getDbConnection()`` para realizar una conexión al gestor y finalmente emplear dicho ``Keeper`` para ejecutar operaciones sobre los modelos.

La clase ``DbConnection``
-------------------------

Esta clase exiente a la clase del mismo nombre que provee Tuza, pero no es más que un envolvente para el espacio de nombre. El grueso de la funcionalidad que Tuza provee se hace por medio de esta clase. Para más detalles consultar la documentación de Tuza.

La clase ``Model``
------------------

Esta clase permite construir una bstracción suave de tablas y vistas. Para cada tabla en una base de datos es posible escribir al menos un modelo que la represeste. Al emplear esta clase lo mínimo a definir es la propiedad ``$_fields`` que es un arreglo que describe todos los campos de la tabla que abstrae.

La clase ``Keeper``
-------------------

Esta clase abstrae la persistencia de datos (operaciones ``CREATE`` y ``UPDATE``) y eliminación de datos, así como diversas operaciones de consulta (operaciones ``SELECT``) en métodos estáticos.
